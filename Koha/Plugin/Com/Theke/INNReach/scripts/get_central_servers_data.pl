#!/usr/bin/perl

#
# Copyright 2019 Theke Solutions
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use DDP;
use Getopt::Long;

use Koha::Plugin::Com::Theke::INNReach;

use Koha::Script;

binmode( STDOUT, ':encoding(utf8)' );

my $agencies;
my $locations;
my $item_types;
my $patron_types;
my $all;

my $result = GetOptions(
    'agencies'     => \$agencies,
    'locations'    => \$locations,
    'item_types'   => \$item_types,
    'patron_types' => \$patron_types,
    'all'          => \$all,
);

unless ($result) {
    print_usage();
    die "Not sure what wen't wrong";
}

unless ( $agencies or $locations or $item_types or $patron_types or $all ) {
    print_usage();
    exit 0;
}

sub print_usage {
    print <<_USAGE_;

    C'mon! Valid options are

    --agencies         Fetch agencies data
    --locations        Fetch locations data
    --item_types       Fetch central item types
    --patron_types     Fetch central patron types
    --all              Fetch all the data from the central servers

_USAGE_
}

if ($all) {
    $agencies     = 1;
    $locations    = 1;
    $item_types   = 1;
    $patron_types = 1;
}

my $response;
my $plugin = Koha::Plugin::Com::Theke::INNReach->new;

my @central_servers = $plugin->central_servers;

if ($agencies) {
    print STDOUT "# Agencies:\n";
    foreach my $central_server (@central_servers) {
        print STDOUT "## $central_server:\n";
        $response = $plugin->contribution($central_server)->get_agencies_list();
        foreach my $agency ( @{$response} ) {
            print STDOUT p($agency);
        }
    }
}

if ($locations) {
    print STDOUT "# Locations:\n";
    foreach my $central_server (@central_servers) {
        print STDOUT "## $central_server:\n";
        $response = $plugin->contribution($central_server)->get_locations_list();
        foreach my $location ( @{$response} ) {
            print STDOUT p($location);
        }
    }
}

if ($item_types) {
    print STDOUT "# Item types:\n";
    foreach my $central_server (@central_servers) {
        print STDOUT "## $central_server:\n";
        $response = $plugin->contribution($central_server)->get_central_item_types();
        foreach my $item_type ( @{$response} ) {
            print STDOUT p($item_type);
        }
    }
}

if ($patron_types) {
    print STDOUT "# Patron types:\n";
    foreach my $central_server (@central_servers) {
        print STDOUT "## $central_server:\n";
        $response = $plugin->contribution($central_server)->get_central_patron_types_list();
        foreach my $patron_type ( @{$response} ) {
            print STDOUT p($patron_type);
        }
    }
}

1;
